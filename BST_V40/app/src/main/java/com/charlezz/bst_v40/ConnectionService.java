package com.charlezz.bst_v40;

import android.app.Service;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Binder;
import android.os.Handler;
import android.os.IBinder;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.Log;

import java.util.List;
import java.util.UUID;

import static com.charlezz.bst_v40.Constant.ACTION_BST_DISCONNECTED;
import static com.charlezz.bst_v40.Constant.ACTION_BST_PHONE_STATE;
import static com.charlezz.bst_v40.Constant.ACTION_BST_PHONE_STATE_CHANGED;
import static com.charlezz.bst_v40.Constant.STATE_IDLE;
import static com.charlezz.bst_v40.Constant.STATE_OFFHOOK;
import static com.charlezz.bst_v40.Constant.STATE_RINGING;
import static com.charlezz.bst_v40.Constant.UUID_ALERT_LEVEL;
import static com.charlezz.bst_v40.Constant.UUID_IMMEDIATE_ALERT;

public class ConnectionService extends Service {
    public static final String TAG = ConnectionService.class.getSimpleName();

    private static final UUID BATTERY_SERVICE_UUID = UUID
            .fromString("0000180F-0000-1000-8000-00805f9b34fb");

    private static final UUID BATTERY_LEVEL_UUID = UUID
            .fromString("00005d0d-0000-1000-8000-00805f9b34fb");

    private final IBinder mBinder = new ConnectionServiceBinder();

    public BluetoothAdapter mBluetoothAdapter;
    public BluetoothManager mBluetoothManager;

    private BluetoothDevice connectedDevice;
    private BluetoothGatt mConnectedGatt;

    private int mState;

    private OnDeviceInfoListener mOnDeviceInfoListener;


    private static final String FORMER_DEVICE_ADDRESS = "former device address";

    PhoneCallReceiver phonCallReceiver;

    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }


    private void registerPhoneCallReceiver() {
//        PackageManager pm = getPackageManager();
//        List<ResolveInfo> resolveInfoList = pm.queryBroadcastReceivers(new Intent(Constant.ACTION_BST_PHONE_STATE_CHANGED), PackageManager.MATCH_DEFAULT_ONLY);
//        for (ResolveInfo resolveInfo : resolveInfoList) {
//            Log.e(TAG, "resolveInfo.resolvePackageName:" + resolveInfo.resolvePackageName);
//        }

        phonCallReceiver = new PhoneCallReceiver();
        IntentFilter filter = new IntentFilter();
        filter.addAction(ACTION_BST_PHONE_STATE_CHANGED);
        registerReceiver(phonCallReceiver, filter);
    }

    private void unregisterPhoneCallReceiver() {
        if (phonCallReceiver != null) {
            unregisterReceiver(phonCallReceiver);
        }
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Log.e(TAG, "Service onCreate");
        AlarmController.getInstance().stopRestarting();
        mBluetoothManager = (BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE);
        mBluetoothAdapter = mBluetoothManager.getAdapter();
        String address = PrefManager.getInstance().getAddress();
        if (!TextUtils.isEmpty(address)) {
            BluetoothDevice device = mBluetoothAdapter.getRemoteDevice(address);
            connect(device);
        }
        registerPhoneCallReceiver();
    }

    public class ConnectionServiceBinder extends Binder {
        ConnectionService getService() {
            return ConnectionService.this;
        }
    }


    public void connect(BluetoothDevice device) {
        if (mConnectedGatt != null) {
            mConnectedGatt.disconnect();
            mConnectedGatt.close();
            mConnectedGatt = null;
        }
        if (connectedDevice != null) {
            connectedDevice = null;
        }
//        device.connectGatt(this, false, mGattCallBack);
        device.connectGatt(this, false, mGattCallBack);
    }

    private BluetoothGattCallback mGattCallBack = new BluetoothGattCallback() {
        @Override
        public void onConnectionStateChange(BluetoothGatt gatt, int status, int newState) {
            super.onConnectionStateChange(gatt, status, newState);
            Log.e(TAG, "onConnectionStateChange:" + newState);

            switch (newState) {
                case BluetoothGatt.STATE_CONNECTED:
                    Log.e(TAG, "STATE_CONNECTED");
                    mConnectedGatt = gatt;
                    mConnectedGatt.discoverServices();
                    mState = BluetoothGatt.STATE_CONNECTED;
                    Log.e(TAG, "mConnectedGatt.readRemoteRssi():" + mConnectedGatt.readRemoteRssi());

                    if (mOnDeviceInfoListener != null) {
                        mOnDeviceInfoListener.OnConnectionChanged(BluetoothGatt.STATE_CONNECTED, gatt.getDevice().getName());
                    }
                    break;
                case BluetoothGatt.STATE_DISCONNECTED:
                    Log.e(TAG, "STATE_DISCONNECTED" + " STATUS:" + status);

                    if (mConnectedGatt != null) {
                        mConnectedGatt.close();
                        mConnectedGatt = null;
                        Intent discoonectedIntent = new Intent(ACTION_BST_DISCONNECTED);
                        sendBroadcast(discoonectedIntent);
                    }
                    mConnectedGatt = null;
                    mState = BluetoothGatt.STATE_DISCONNECTED;
                    if (mOnDeviceInfoListener != null) {
                        mOnDeviceInfoListener.OnConnectionChanged(BluetoothGatt.STATE_DISCONNECTED, gatt.getDevice().getName());
                    }

                    break;
            }
        }

        @Override
        public void onServicesDiscovered(BluetoothGatt gatt, int status) {
            super.onServicesDiscovered(gatt, status);
            if (status == BluetoothGatt.GATT_SUCCESS) {
                if (gatt != null) {
                    List<BluetoothGattService> services = gatt.getServices();
                    for (BluetoothGattService service : services) {
                        for (BluetoothGattCharacteristic characteristic : service.getCharacteristics()) {
                            mConnectedGatt.setCharacteristicNotification(characteristic, true);
                            if (characteristic.getUuid().equals(BATTERY_LEVEL_UUID)) {
                                mConnectedGatt.readCharacteristic(characteristic);
                            }
                        }
                    }
                }
            } else {
                Log.e(TAG, "onServicesDiscovered received: " + status);
            }
        }

        @Override
        public void onCharacteristicRead(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status) {
            super.onCharacteristicRead(gatt, characteristic, status);
            if (characteristic.getUuid().equals(BATTERY_LEVEL_UUID)) {
                if (status == BluetoothGatt.GATT_SUCCESS) {
                    int value = characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT8, 0);
                    if (mOnDeviceInfoListener != null) {
                        mOnDeviceInfoListener.onBatterChanged(value);
                    }
                }


            }

        }

        @Override
        public void onReadRemoteRssi(final BluetoothGatt gatt, int rssi, int status) {
            super.onReadRemoteRssi(gatt, rssi, status);

            Log.w(TAG, "onReadRemoteRssi:" + rssi);
            mHandler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    gatt.readRemoteRssi();
                }
            }, 1000);
//            gatt.readRemoteRssi();

            if (mOnDeviceInfoListener != null) {
                mOnDeviceInfoListener.onReadRssi(rssi);
            }
        }

        @Override
        public void onCharacteristicChanged(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic) {
            super.onCharacteristicChanged(gatt, characteristic);
            if (characteristic.getUuid().equals(BATTERY_LEVEL_UUID)) {
                if (mOnDeviceInfoListener != null) {
                    mOnDeviceInfoListener.onBatterChanged(characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT8, 0));
                }
            }
        }
    };

    public void setOnDeviceInfoListener(OnDeviceInfoListener listener) {
        mOnDeviceInfoListener = listener;
    }

    @Override
    public void onDestroy() {
        unregisterPhoneCallReceiver();
        Log.e(TAG, "Service onDestroy");
        if (mState == BluetoothGatt.STATE_CONNECTED) {
            String address = mConnectedGatt.getDevice().getAddress();
            Log.e(TAG, "FORMER_DEVICE:" + address);
            Intent intent = new Intent(MyApp.getContext(), ConnectionService.class);
            intent.putExtra(FORMER_DEVICE_ADDRESS, address);
            AlarmController.getInstance().restartService(intent, 0, 10);
        }
        super.onDestroy();


    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.e(TAG, "onStartCommand");
//        if (intent != null && intent.hasExtra(FORMER_DEVICE_ADDRESS)) {
//            String address = intent.getStringExtra(FORMER_DEVICE_ADDRESS);
//            Log.e(TAG, "FORMER_DEVICE_ADDRESS:" + address);
//            BluetoothDevice device = mBluetoothAdapter.getRemoteDevice(address);
//            connect(device);
//        }

        return Service.START_STICKY;
    }

    private Handler mHandler = new Handler();


    private class PhoneCallReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {

            if (mConnectedGatt == null) {
                return;
            }

            BluetoothGattCharacteristic characteristic = mConnectedGatt.getService(UUID_IMMEDIATE_ALERT).getCharacteristic(UUID_ALERT_LEVEL);

            String action = intent.getAction();
            if (action.equals(ACTION_BST_PHONE_STATE_CHANGED)) {
                String state = intent.getStringExtra(ACTION_BST_PHONE_STATE);
                if (state.equals(TelephonyManager.EXTRA_STATE_RINGING)) {
                    Log.e(TAG, "RINGING");
                    characteristic.setValue(STATE_RINGING, BluetoothGattCharacteristic.FORMAT_UINT8, 0);
                } else if (state.equals(TelephonyManager.EXTRA_STATE_IDLE)) {
                    Log.e(TAG, "IDLE");
                    characteristic.setValue(STATE_IDLE, BluetoothGattCharacteristic.FORMAT_UINT8, 0);
                } else if (state.equals(TelephonyManager.EXTRA_STATE_OFFHOOK)) {
                    Log.e(TAG, "OFFHOOK");
                    characteristic.setValue(STATE_OFFHOOK, BluetoothGattCharacteristic.FORMAT_UINT8, 0);
                }
                mConnectedGatt.writeCharacteristic(characteristic);
            }
        }
    }
}
