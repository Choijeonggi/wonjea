package com.charlezz.bst_v40;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.WindowManager;

import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class AlarmActivity extends Activity {


    Unbinder unbinder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON
                | WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD
                | WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON
                | WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED);
        setContentView(R.layout.activity_alarm);
        unbinder = ButterKnife.bind(this);

    }

    @Override
    protected void onResume() {
        super.onResume();
        MediaPlayerController.getInstance().start();
        VibrationController.getInstance().start();
    }

    @Override
    protected void onPause() {
        super.onPause();
        MediaPlayerController.getInstance().stop();
        VibrationController.getInstance().cancel();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unbinder.unbind();
    }

    @OnClick(R.id.retry)
    public void retry() {
        finish();
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.cancel)
    public void cancel() {
        finish();
    }
}
