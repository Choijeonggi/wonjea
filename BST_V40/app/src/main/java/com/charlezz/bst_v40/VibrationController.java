package com.charlezz.bst_v40;

import android.content.Context;
import android.os.Vibrator;

/**
 * Created by Charles on 2017. 3. 5..
 */
public class VibrationController {

    public static final String TAG = VibrationController.class.getSimpleName();

    private static VibrationController ourInstance = new VibrationController();

    public static VibrationController getInstance() {
        return ourInstance;
    }

    private Vibrator vibrator;

    private VibrationController() {
        vibrator = (Vibrator) MyApp.getContext().getSystemService(Context.VIBRATOR_SERVICE);
    }

    public void start() {
        long[] pattern = new long[]{500, 500};
        vibrator.vibrate(pattern, 0);
    }

    public void cancel(){
        vibrator.cancel();
    }
}
