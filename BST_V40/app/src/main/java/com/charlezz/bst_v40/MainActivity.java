package com.charlezz.bst_v40;

import android.Manifest;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.github.lzyzsd.circleprogress.ArcProgress;
import com.github.lzyzsd.circleprogress.CircleProgress;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import pub.devrel.easypermissions.EasyPermissions;

public class MainActivity extends AppCompatActivity implements EasyPermissions.PermissionCallbacks {
    public static final String TAG = MainActivity.class.getSimpleName();

    private static final int MENU_DEFAULT_GROUP = 0;
    private static final int MENU_SCAN_ACTIVITY = 0;

    private static final int REQ_PERMISSION = 0;
    private static final int REQ_ENABLE_BT = 1;
    private static final int REQ_NEW_DEVICE = 2;


    private ConnectionService mService;
    private boolean mBound = false;
    private Unbinder butterKnifeBinder;

    public BluetoothAdapter mBluetoothAdapter;
    public BluetoothManager mBluetoothManager;

    public BluetoothDevice newDevice;

    @BindView(R.id.signal_progress)
    CircleProgress signalProgress;
    @BindView(R.id.battery_progress)
    ArcProgress batteryProgress;


    private static final String[] permisiions = new String[]{
            Manifest.permission.BLUETOOTH,
            Manifest.permission.BLUETOOTH_ADMIN,
            Manifest.permission.ACCESS_COARSE_LOCATION,
            Manifest.permission.READ_PHONE_STATE};


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Log.w(TAG, "onCreate");
        butterKnifeBinder = ButterKnife.bind(this);
        getSupportActionBar().setTitle("No Connection");
    }

    @Override
    protected void onResume() {
        super.onResume();
        requestPermission();
    }

    @Override
    protected void onPause() {
        Log.e(TAG, "onPause");
        super.onPause();
        if (mBound) {
            Log.e(TAG, "unbindService");
            unbindService(mConnection);
            mService.setOnDeviceInfoListener(null);
            mService = null;
            mBound = false;
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.e(TAG, "onDestroy");
        butterKnifeBinder.unbind();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this);
    }

    //Declare Service Connection
    private ServiceConnection mConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            Log.e(TAG, "onServiceConnected");
            mBound = true;
            ConnectionService.ConnectionServiceBinder binder = (ConnectionService.ConnectionServiceBinder) service;
            mService = binder.getService();
            mService.setOnDeviceInfoListener(infoListener);

            initBluetooth();
            if (newDevice != null) {
                mService.connect(newDevice);
            }
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            Log.e(TAG, "onServiceDisconnected");
            mBound = false;
            getSupportActionBar().setTitle("No Connection");
        }
    };


    private void bindConnectionService() {
        Log.e(TAG, "bindConnectionService");
        if (getPackageManager().hasSystemFeature(PackageManager.FEATURE_BLUETOOTH_LE)) {
            if (!mBound) {
                Intent intent = new Intent(MainActivity.this, ConnectionService.class);
//                startService(intent);
                bindService(intent, mConnection, BIND_AUTO_CREATE);
            }
        } else {
            finish();
        }

    }


    private void requestPermission() {

        if (EasyPermissions.hasPermissions(this, permisiions
        )) {
            Log.e(TAG, "hasPermissions");
            bindConnectionService();
        } else {
            EasyPermissions.requestPermissions(
                    this,
                    "Permission",
                    REQ_PERMISSION,
                    permisiions
            );
        }
    }

    @Override
    public void onPermissionsGranted(int requestCode, List<String> perms) {
        Log.e(TAG, "onPermissionsGranted");
        bindConnectionService();
    }

    @Override
    public void onPermissionsDenied(int requestCode, List<String> perms) {
        Log.e(TAG, "onPermissionsDenied");

        for (String permission : perms) {
            Log.e(TAG, requestCode + "|" + permission);
        }
        finish();
    }

    protected void initBluetooth() {
        if (!getPackageManager().hasSystemFeature(PackageManager.FEATURE_BLUETOOTH_LE)) {
            Toast.makeText(this, "Bluetooth is not supported", Toast.LENGTH_SHORT).show();
            finish();
        }

        mBluetoothManager = (BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE);
        mBluetoothAdapter = mBluetoothManager.getAdapter();


        //if Bluetooth is off the request turning on
        if (mBluetoothAdapter == null || !mBluetoothAdapter.isEnabled()) {
            Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableBtIntent, REQ_ENABLE_BT);
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.e(TAG, "onActivityResult");
        if (requestCode == REQ_ENABLE_BT) {
            if (resultCode != RESULT_OK) {
                finish();
            }
        } else if (requestCode == REQ_NEW_DEVICE && resultCode == RESULT_OK) {
            getSupportActionBar().setTitle("Connecting");
            String name = data.getStringExtra(ScanActivity.DEVICE_NAME);
            String address = data.getStringExtra(ScanActivity.DEVICE_ADDRESS);

            Log.e(TAG, "name:" + name);
            Log.e(TAG, "address:" + address);

            PrefManager.getInstance().setAddress(address);
            newDevice = mBluetoothAdapter.getRemoteDevice(address);
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuItem scanItem = menu.add(MENU_DEFAULT_GROUP, MENU_SCAN_ACTIVITY, MENU_SCAN_ACTIVITY, "New");
        scanItem.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case MENU_SCAN_ACTIVITY:
                startActivityForResult(new Intent(MainActivity.this, ScanActivity.class), REQ_NEW_DEVICE);
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private OnDeviceInfoListener infoListener = new OnDeviceInfoListener() {
        @Override
        public void onReadRssi(final int rssi) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    signalProgress.setProgress(Math.abs(rssi));
                }
            });

        }

        @Override
        public void onBatterChanged(final int level) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    batteryProgress.setProgress(level);
                }
            });

        }

        @Override
        public void OnConnectionChanged(final int state, final String name) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    switch (state) {
                        case BluetoothGatt.STATE_CONNECTED:
                            if (TextUtils.isEmpty(name)) {
                                getSupportActionBar().setTitle("Unknown");
                            } else {
                                getSupportActionBar().setTitle(name);
                            }
                            break;
                        case BluetoothGatt.STATE_DISCONNECTED:
                            getSupportActionBar().setTitle("No Connection");
                            break;

                    }
                }
            });
        }
    };


}
