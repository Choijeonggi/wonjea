package com.charlezz.bst_v40;

import java.util.UUID;

/**
 * Created by Charles on 2017. 3. 5..
 */

public class Constant {

    public static final String ACTION_BST_PHONE_STATE = "action_bst_phone_state";
    public static final String ACTION_BST_PHONE_STATE_CHANGED = "action_bst_phone_state_changed";
    public static final String ACTION_BST_DISCONNECTED = "action_bst_disconnected";

    public static final UUID UUID_IMMEDIATE_ALERT = UUID.fromString("00001802-0000-1000-8000-00805f9b34fb");
    public static final UUID UUID_ALERT_LEVEL = UUID.fromString("00002A06-0000-1000-8000-00805f9b34fb");

    public static final int STATE_IDLE = 0;
    public static final int STATE_RINGING = 1;
    public static final int STATE_OFFHOOK = 2;
}
