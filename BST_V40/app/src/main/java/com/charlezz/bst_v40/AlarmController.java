package com.charlezz.bst_v40;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

/**
 * Created by Charles on 2/27/17.
 */
public class AlarmController {

    public static String TAG = AlarmController.class.getSimpleName();

    private static AlarmController ourInstance = new AlarmController();

    public static AlarmController getInstance() {
        return ourInstance;
    }

    private AlarmManager manager;
    private PendingIntent pi;

    private AlarmController() {
        manager = (AlarmManager) MyApp.getContext().getSystemService(Context.ALARM_SERVICE);
    }

    public void restartService(Intent intent, int trigger, int interval) {
        Log.e(TAG, "restartService");
        pi = PendingIntent.getService(MyApp.getContext(), 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        manager.setRepeating(AlarmManager.RTC_WAKEUP, System.currentTimeMillis() + trigger * 1000, interval * 1000, pi);
    }

    public void stopRestarting() {
        if (pi != null) {
            manager.cancel(pi);
        }

    }
}
